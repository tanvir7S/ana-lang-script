# analytics-lang-script

Using this script, we can get the 'self::TRANSLATIONS' array for migrations involving adding new translations.

# Steps to follow

1.Download .csv copies of translation files (files with language message and definition)

2.Rename the files to language shorthand/abbreviations, i.e, [For Greek Language Translation File --> el.csv, For SouthKorean Language Translation File --> ko.csv]

3.Place the renamed .csv files into 'inputs/' directory

4.Run main.php with params in the terminal.
	-> Params with explaination:
	
	4.1 'l' --> language(s) (Required)
	4.2 'e' ---> 'column number for event names' (Required)
	4.3 'm' ---> 'column number for langugage message' (Required)
	4.4 'd' ---> 'column number for langugage definition' (Optional : Do not provide if .csv file has no language definition column)
	4.5 's' ---> 'start row number for values' (Required)

5.The resulting array can be found in 'output.txt'. Just Copy & use it for 'self::TRANSLATIONS' array in migration file.

# Sample Command

With Language Definition Column
--------------------------------------------

php main.php -lel -lko -e0 -m3 -d4 -s2 


Without Language Definition Column
--------------------------------------------

php main.php -lel -lko -e0 -m2 -s2 