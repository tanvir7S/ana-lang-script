<?php

require __DIR__ . '/functions.php';

$val = getopt("l:e:m:d:s:");

$startRow = $val['s'];
$languageMsgColumn = $val['m'];
$languageDefColumn = $val['d'] ?? -1;
$eventValueColumn = $val['e'];
$languages =  $val['l'];

$fileDir = 'inputs/';
$languageTranslations = [];
$events = [];

if (($startRow==null) || ($languageMsgColumn==null) || ($eventValueColumn==null) || (empty($languages))) {
    echo 'Missing Required Param (l, e, m or s)';
    die;
}

$events = getEventsFromCsv($fileDir, $languages[0], $startRow, $eventValueColumn);
$languageTranslations = getMessageAndDefintionFromCsv($fileDir, $languages, $startRow, $languageMsgColumn, $languageDefColumn);
$result = getResultingArray($languages, $events, $languageTranslations);
$myOutputStr = getOutputString($result);

$file = 'output.txt';
file_put_contents($file, $myOutputStr);
