<?php

function getEventsFromCsv($fileDir, $fileName, $startRow, $eventValueColumn) {
    $events = [];
    $row = 0;
    if (($handle = fopen($fileDir . $fileName . ".csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            if($row>=$startRow){
                $events[] = trim($data[$eventValueColumn]);
            }
            $row++;
        }
        fclose($handle);
    }

    return $events;
}

function getMessageAndDefintionFromCsv($fileDir, $languages, $startRow, $languageMsgColumn, $languageDefColumn = -1) {
    $languageTranslations = [];

    foreach ($languages as $language){
        $row = 0;
        $i=0;
        if (($handle = fopen($fileDir . $language . ".csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                if($row>=$startRow){
                    $languageTranslations[$language][$i]['msg'] = trim($data[$languageMsgColumn]);
                    if($languageDefColumn >=0 ){
                        $languageTranslations[$language][$i]['def'] = trim($data[$languageDefColumn]);
                    }else{
                        $languageTranslations[$language][$i]['def'] = 'null';
                    }
                    $i++;
                }
                $row++;
            }
            fclose($handle);
        }
    }

    return $languageTranslations;
}

function getResultingArray($languages, $events, $languageTranslations){
    $result = [];
    $i=0;

    foreach ($events as $key){
        foreach ($languages as $language){
            $result[$key][$language]['self::DML_VERB'] = "'insert'";
            $result[$key][$language]['self::MESSAGE']  = "\"" . $languageTranslations[$language][$i]['msg'] . "\"";
            $def = $languageTranslations[$language][$i]['def'];
            $result[$key][$language]['self::DEFINITION'] = ($def=='null') ? $def :  "\"" . $def . "\"";;
        }
        $i++;
    }

    return $result;
}

function getOutputString($result){
    $myOutput = "[\n";

    foreach ($result as $eventKey => $languageArr){
        $myOutput .= "\"" . $eventKey . "\"" . " => [\n";
        $myOutput .= "\t";
        foreach ($languageArr as $languageKey => $languageValues){
            $myOutput .= "'" . $languageKey . "'" . " => [\n";
            foreach ($languageValues as $key => $value){
                $myOutput .= "\t\t";
                $myOutput .= $key . ' => ' . $value . ",\n";
            }
            $myOutput .= "\t],";
            $myOutput .= "\n";
            $myOutput .= "\t";
        }
        $myOutput .= "\n],";
        $myOutput .= "\n";
    }
    $myOutput .= "\n],";

    return $myOutput;
}